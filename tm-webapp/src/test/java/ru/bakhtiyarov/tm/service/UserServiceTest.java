package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.config.WebMvcConfiguration;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.util.HashUtil;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class UserServiceTest {

    @Autowired
    private IUserService userService;

    @NotNull
    private User user1;

    @NotNull
    private User user2;

    @Before
    public void initData() {
        user1 = new User("login", "pass", Role.USER);
        user2 = new User("login1", "pass1", Role.USER);
        userService.save(user1);
        userService.save(user2);
    }

    @After
    public void deleteData() {
        userService.removeAll();
    }

    @Test
    public void testCreateLoginPasswordEmail(){
        Assert.assertEquals(2, userService.findAll().size());
        @Nullable final User newUser = userService.create("new login", "password", "email");
        Assert.assertNotNull(newUser);
        Assert.assertEquals(3, userService.findAll().size());
        Assert.assertEquals(newUser.getLogin(), "new login");
        Assert.assertEquals(newUser.getEmail(), "email");
        Assert.assertEquals(newUser.getRole(), Role.USER);
    }

    @Test
    public void testCreateLoginPasswordRole() {
        Assert.assertEquals(2, userService.findAll().size());
        @Nullable final User user = userService.create("new login", "password", Role.ADMIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(3, userService.findAll().size());
        Assert.assertEquals(user.getLogin(), "new login");
        Assert.assertEquals(user.getRole(), Role.ADMIN);
    }

    @Test
    public void testUpdatePassword() {
        @Nullable final User nullUser = userService.updatePassword("false userID", "new pass");
        Assert.assertNull(nullUser);
        @Nullable final User testUser = userService.updatePassword(user1.getId(), "new pass");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(HashUtil.salt("new pass"), testUser.getPasswordHash());
    }

    @Test
    public void testUpdateUserEmail() {
        @Nullable final User nullUser = userService.updateUserEmail("false userID", "email");
        Assert.assertNull(nullUser);
        @Nullable final User testUser = userService.updateUserEmail(user1.getId(), "new email");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getEmail(), "new email");
    }

    @Test
    public void testUpdateUserLogin() {
        @Nullable final User nullUser = userService.updateUserLogin("false userID", "login");
        Assert.assertNull(nullUser);
        @Nullable final User testUser = userService.updateUserLogin(user1.getId(), "new login");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getLogin(), "new login");
    }

    @Test
    public void testUpdateFirstName() {
        @Nullable final User nullUser = userService.updateUserFirstName("false userID", "first name");
        Assert.assertNull(nullUser);
        @Nullable final User testUser = userService.updateUserFirstName(user1.getId(), "new first name");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getFirstName(), "new first name");
    }

    @Test
    public void testUpdateLastName() {
        @Nullable final User nullUser = userService.updateUserLastName("false userID", "last name");
        Assert.assertNull(nullUser);
        @Nullable final User testUser = userService.updateUserLastName(user1.getId(), "new last name");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getLastName(), "new last name");
    }

    @Test
    public void testUpdateMiddleName() {
        @Nullable final User nullUser = userService.updateUserMiddleName("false userID", "mid name");
        Assert.assertNull(nullUser);
        @Nullable final User testUser = userService.updateUserMiddleName(user1.getId(), "new middle name");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getMiddleName(), "new middle name");
    }

    @Test
    public void testFindByLogin() {
        @Nullable final User testUser = userService.findByLogin(user1.getLogin());
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getId(), user1.getId());
        Assert.assertEquals(testUser.getLogin(), user1.getLogin());
    }

    @Test
    public void testLockUserByLogin() {
        Assert.assertEquals(user1.getLocked(), false);
        userService.lockUserByLogin(user1.getLogin());
        final @Nullable User testUser = userService.findByLogin(user1.getLogin());
        Assert.assertEquals(testUser.getId(), user1.getId());
        Assert.assertEquals(testUser.getLocked(), true);
    }

    @Test
    public void testUnLockedUserByLogin() {
        userService.lockUserByLogin(user1.getLogin());
        @Nullable User testUser = userService.findByLogin(user1.getLogin());
        Assert.assertEquals(testUser.getId(), user1.getId());
        Assert.assertEquals(testUser.getLocked(), true);
        userService.unLockUserByLogin(user1.getLogin());
        testUser = userService.findByLogin(user1.getLogin());
        Assert.assertEquals(testUser.getLocked(), false);
    }

    @Test
    public void testRemoveByLogin() {
        Assert.assertEquals(2, userService.findAll().size());
        userService.removeByLogin(user1.getLogin());
        Assert.assertEquals(1, userService.findAll().size());
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(2, userService.findAll().size());
        userService.removeById(user1.getId());
        Assert.assertEquals(1, userService.findAll().size());
    }

    @Test
    public void testRemoveAll() {
        Assert.assertEquals(2, userService.findAll().size());
        userService.removeAll();
        Assert.assertEquals(0, userService.findAll().size());
    }

}