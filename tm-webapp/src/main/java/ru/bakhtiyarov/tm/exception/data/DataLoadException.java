package ru.bakhtiyarov.tm.exception.data;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class DataLoadException extends AbstractException {

    @NotNull
    public DataLoadException() {
        super("An error occurred while loading data");
    }

    @NotNull
    public DataLoadException(Throwable cause) {
        super(cause);
    }

}
