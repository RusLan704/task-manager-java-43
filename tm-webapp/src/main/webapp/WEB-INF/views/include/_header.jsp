<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>TASK-MANAGER</title>
</head>
<style>
    h1 {
        font-size: 1.6em;
    }

    a {
        color: darkblue;
    }

</style>
<body>
<table border="3" width="1100px" align="center" style="border-collapse: collapse">
    <tr>
        <td width="200" height="35" nowrap="nowrap" align="center"><b>TASK MANAGER</b></td>
        <td width="100%" align="right">

            <a href="/projects/list">PROJECTS</a>
            &nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/tasks/list">TASKS</a>
             &nbsp;&nbsp;|&nbsp;&nbsp;

            <sec:authorize access="isAuthenticated()">
                USER: <sec:authentication property="name"/>
                &nbsp;&nbsp;
                <a href="/logout">LOGOUT</a>
                &nbsp;&nbsp;
            </sec:authorize>

            <sec:authorize access="!isAuthenticated()">
                <a href="/login">LOGIN</a>
                &nbsp;&nbsp;
            </sec:authorize>

        </td>
    </tr>
    <tr>
        <td colspan="2" height="100%" valign="top" style="padding: 10px;">