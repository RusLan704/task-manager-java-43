package io.swagger.tm_client.exception.system;

import org.jetbrains.annotations.NotNull;
import io.swagger.tm_client.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    @NotNull
    public UnknownArgumentException(@NotNull final String arg) {
        super("Error! Command ``" + arg + "`` doesn't exist...");
    }

}