package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.ProjectDTO;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProjectRestControllerTest {

    private final DefaultApi api = new DefaultApi();

    @Before
    public void initData() throws ApiException {
        api.login("test", "test");
    }

    @After
    public void clearData() throws ApiException {
        api.logout();
    }

    @Test
    public void testFindAll() throws ApiException {
        Assert.assertTrue(api.findAll().size() > 0);
    }

    @Test
    public void createTest() throws ApiException {
        final ProjectDTO project = new ProjectDTO();
        project.setId("123");
        project.setName("test");
        project.setDescription("test");
        api.create(project);
        ProjectDTO tempProject = api.findById("123");
        Assert.assertEquals(tempProject.getName(), "test");
        Assert.assertEquals(tempProject.getDescription(), "test");
        api.removeOneById("123");
    }

    @Test
    public void testRemove() throws ApiException {
        final ProjectDTO project = new ProjectDTO();
        project.setId("123");
        project.setName("test");
        project.setDescription("test");
        api.create(project);
        api.removeOneById("123");
        Assert.assertFalse(api.findAll().contains(project));
    }

    @Test
    public void testUpdate() throws ApiException {
        final ProjectDTO project = new ProjectDTO();
        project.setId("123");
        project.setName("name");
        project.setDescription("description");
        api.create(project);
        project.setName("new name");
        project.setDescription("new description");
        api.updateProjectById(project);
        ProjectDTO projectUpdated = api.findById("123");
        Assert.assertEquals(projectUpdated.getName(), "new name");
        Assert.assertEquals(projectUpdated.getDescription(), "new description");
        api.removeOneById("123");
    }

}
